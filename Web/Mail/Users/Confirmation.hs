module Web.Mail.Users.Confirmation where
import Web.View.Prelude
import IHP.MailPrelude
import IHP.AuthSupport.Confirm

instance BuildMail (ConfirmationMail User) where
    subject = "Confirm your Account"
    to ConfirmationMail { .. } = Address { addressName = Nothing, addressEmail = get #email user }
    from = "aaron.alef@code.berlin"
    html ConfirmationMail { .. } = [hsx|
            Please click the following link to confirm account creation:<br/><br/>

            {urlTo (ConfirmUserAction (get #id user) confirmationToken)}
    |]

