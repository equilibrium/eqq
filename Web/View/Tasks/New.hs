module Web.View.Tasks.New where
import Web.View.Prelude

data NewView = NewView
        { task :: Task
        }

instance View NewView where
    html NewView { .. } = [hsx|
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href={TasksAction}>Tasks</a></li>
                <li class="breadcrumb-item active">New Task</li>
            </ol>
        </nav>
        <h1>New Task</h1>
        {renderForm task}
    |]

renderForm :: Task -> Html
renderForm task = formFor task [hsx|
        {renderRequiredLabel "Title"}
        {(textField #title) 
            { required  = True }
            { autofocus = True }
            { disableLabel = True }
        }
        {(textareaField #description) 
            { placeholder = "Description..." }
            { disableLabel = True }
        }
        {(hiddenField #done)}
        {(dateTimeField #dueAt)}
        {submitButton 
            { label = "Schedule" }
        }
    |]
