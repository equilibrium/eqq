module Web.View.Tasks.Show where
import Web.View.Prelude
import Web.Controller.Prelude ( getRequestPathAndQuery )

import Data.UUID ( nil )

import qualified Text.MMark as MD

data ShowView = ShowView { task         ::  Task
                         , dependencies :: [Task]
                         , dependees    :: [Task]
                         }

instance View ShowView where
    html ShowView { .. } = [hsx|
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href={TasksAction}>Tasks</a></li>
                <li class="breadcrumb-item active">Show Task</li>
            </ol>
        </nav>
        <h1>{get #title task}</h1>
        <table style="color:black">
            <tr>
                <th>Status</th>
                <td id={statusId task}>
                    <a href={pathTo $ ToggleStatusAction (get #id task) (textBody $ show getRequestPathAndQuery)} data-turbolinks="false">
                        {divStatus task}
                    </a>
                </td>
            </tr>
            <tr>
                <th>Due at</th>
                <td>{get #dueAt task}</td>
            </tr>
            <tr>
                <th><a href={EditTaskAction (get #id task)} style="color:black">Description</a></th>
            </tr>
        </table>
        {get #description task |> renderMarkdown}
        {renderDeps (get #id task) dependencies dependees}

    |]

renderMarkdown text = 
    case text |> MD.parse "" of
        Left error -> "Could not render Markdown"
        Right markdown -> MD.render markdown |> tshow |> preEscapedToHtml


renderDeps :: Id Task -> [Task] -> [Task] -> Html
renderDeps taskId dependencies dependees = [hsx|
    <table width="100%">
        <tr>
            <th>
                <a href={pathTo $ AddRelationAction taskId (Id nil)}>
                    +
                </a>
                Dependencies
            </th>
            <th>
                <a href={pathTo $ AddRelationAction (Id nil) taskId}>
                    +
                </a>
                Dependees
            </th>
        </tr>
        <tr style="vertical-align: top">
            <td>
                <table width="95%">
                    <colgroup>
                        <col span="1" style="width: 0%">
                        <col span="1" style="width: 27px">
                    </colgroup>
                    {forEach dependencies (renderDependency taskId)}
                </table>
            </td>
            <td>
                <table width="95%">
                    <colgroup>
                        <col span="1" style="width: 0%">
                        <col span="1" style="width: 27px">
                    </colgroup>
                    {forEach dependees (renderDependee taskId)}
                </table>
            </td>
        </tr>
    </table>
|]

renderDependency :: Id Task -> Task -> Html
renderDependency dependeeId dependency = [hsx|
    <tr>
        <td id={statusId dependency}>
            <a href={ShowTaskAction (get #id dependency)} id={statusId dependency} data-turbolinks="false">
                <div style="text-align: left">
                    {get #title dependency}
                </div>
            </a>
        </td>
        <td>
            <a href={DeleteRelationAction dependeeId (get #id dependency)} style="border-radius: 0" class="btn btn-danger js-delete">
                <div>
                    X
                </div>
            </a>
        </td>
    </tr>
|]

renderDependee :: Id Task -> Task -> Html
renderDependee dependencyId dependee = [hsx|
    <tr>
        <td id={statusId dependee}>
            <a href={ShowTaskAction (get #id dependee)} id={statusId dependee} data-turbolinks="false">
                <div style="text-align: left">
                    {get #title dependee}
                </div>
            </a>
        </td>
        <td>
            <a href={DeleteRelationAction (get #id dependee) dependencyId} style="border-radius: 0" class="btn btn-danger js-delete">
                <div>
                    X
                </div>
            </a>
        </td>
    </tr>
|]


