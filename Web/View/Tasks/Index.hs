module Web.View.Tasks.Index where
import Web.View.Prelude
import Web.Controller.Prelude ( getRequestPathAndQuery )

import Data.List ( sortBy )

data IndexView = IndexView { tasks :: [Task] }

instance View IndexView where
    html IndexView { .. } = [hsx|
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    Tasks
                </li>
                <li class="breadcrumb-item">
                    <a href={pathTo EditUserAction}>
                        Edit
                    </a>
                </li>
                <li class="breadcrumb-item">
                    <a href={pathTo DeleteSessionAction} class="js-delete">
                        Logout
                    </a>
                </li>
            </ol>
        </nav>
        <h1>Tasks
            <a href={pathTo NewTaskAction} class="btn btn-primary ml-4">+ New</a>
            <a href={pathTo NewRelationAction} class="btn btn-info ml-4">+ Relation</a>
        </h1>
        <div class="table-responsive">
            <table style="border: 1px solid">
                <colgroup>
                    <col span="1" style="width: 70%">
                    <col span="1" style="width: 20%">
                </colgroup>
                <thead style="border: 1px solid">
                    <tr>
                        <th><a href={pathTo $ SortedTasksAction "title"} data-turbolinks="false">
                            <div>Title</div>
                        </a></th>
                        <th><a href={pathTo $ SortedTasksAction "due_at"} data-turbolinks="false">
                            <div>due at</div>
                        </a></th>
                        <th><a href={pathTo $ SortedTasksAction "status"} data-turbolinks="false">
                            <div>Status</div>
                        </a></th>
                    </tr>
                </thead>
                <tbody>{forEach tasks renderTask}</tbody>
            </table>
        </div>
    |]

renderTask task = [hsx|
    <tr style="border: 1px solid">
        <td>
            <a href={ShowTaskAction (get #id task)}>
                <div>
                    {get #title task}
                </div>
            </a>
        </td>
        <td>
            {get #dueAt task}
        </td>
        <td id={statusId task}>
            <a href={ToggleStatusAction (get #id task) (textBody $ show getRequestPathAndQuery)} data-turbolinks="false">
                {divStatus task}
            </a>
        </td>
        <td>
            <a href={EditTaskAction (get #id task)} class="text-muted">
                Edit
            </a>
        </td>
        <td>
            <a href={DeleteTaskAction (get #id task)} style="color: red" class="js-delete">
                X
            </a>
        </td>
    </tr>
|]


