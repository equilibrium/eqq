module Web.View.Tasks.Edit where
import Web.View.Prelude

data EditView = EditView { task :: Task }

instance View EditView where
    html EditView { .. } = [hsx|
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href={TasksAction}>Tasks</a></li>
                <li class="breadcrumb-item active">Edit Task</li>
            </ol>
        </nav>
        <h1>Edit Task</h1>
        {renderForm task}
        <br/>
        <a href={pathTo (DeleteTaskAction (get #id task))} class="btn btn-danger js-delete">Delete</a>
    |]

renderForm :: Task -> Html
renderForm task = formFor task [hsx|
    {renderRequiredLabel "Title"}
    {(textField #title)
        { required     = True }
        { disableLabel = True }
    }
    {(textareaField #description)
        { placeholder  = "Description..." }
        { autofocus    = True }
        { disableLabel = True }
    }
    {renderRequiredLabel "Status"}
    {(selectField #done [False, True])
        { required     = True }
        { disableLabel = True }
    }
    {(dateTimeField #dueAt)}
    {submitButton { label = "Reschedule" } }
|]
