module Web.View.User.Edit where
import Web.View.Prelude

data EditView = EditView { user :: User }

instance View EditView where
    html EditView { .. } = [hsx|
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <a href={pathTo TasksAction}>
                        Tasks
                    </a>
                </li>
                <li class="breadcrumb-item active">
                    Edit
                </li>
                <li class="breadcrumb-item active">
                    <a href={pathTo DeleteUserAction} class="js-delete" style="color:#FF0000">
                        Delete Account
                    </a>
                </li>
            </ol>
        </nav>
        <h1>Edit User</h1>
        {renderForm user}
    |]

renderForm :: User -> Html
renderForm user = [hsx|
    <form method="POST" action={UpdateUserAction}>
    {renderRequiredLabel "Email"}
        <div class="form-group">
            <input name="email" type="email" class="form-control" placeholder="New email" required>
        </div>
        <span>Password</span>
        <div class="form-group">
            <input name="password" type="password" class="form-control" placeholder="New password"/>
        </div>
        <div class="form-group">
            <input name="confirmation" type="password" class="form-control" placeholder="Repeat password"/>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
|]
