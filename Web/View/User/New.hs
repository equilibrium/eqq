module Web.View.User.New where
import Web.View.Prelude

data NewView = NewView { user :: User }

instance View NewView where
    html NewView { .. } = [hsx|
        <div class="h-100" id="sessions-new">
            <div class="d-flex align-items-center">
                <div class="w-100">
                    <div style="max-width: 400px" class="mx-auto mb-5">
                        <h5>Register</h5>
                        {renderForm user}
                    </div>
                </div>
            </div>
        </div>
    |]

renderForm :: User -> Html
renderForm user = formFor user [hsx|
    <form method="POST" action={CreateSessionAction}>
        {(emailField #email)
            { required     = True }
            { autofocus    = True }
            { disableLabel = True }
            { placeholder  = "E-Mail" }
        }
        {(passwordField #passwordHash)
            { required     = True }
            { disableLabel = True }
            { placeholder  = "Password" }
        }
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Register</button>
        </div>
    </form>
|]
