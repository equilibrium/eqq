module Web.View.Relations.Index where
import Web.View.Prelude

data IndexView = IndexView { relations :: [Relation] }

instance View IndexView where
    html IndexView { .. } = [hsx||]
