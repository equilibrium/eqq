module Web.View.Relations.New where
import Web.View.Prelude

data NewView = NewView { relation :: Relation, tasks :: [Task] }

instance View NewView where
    html NewView { .. } = [hsx|
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href={RelationsAction}>Relations</a></li>
                <li class="breadcrumb-item active">New Relation</li>
            </ol>
        </nav>
        <h1>New Relation</h1>
        {renderForm relation tasks}
    |]

renderForm :: Relation -> [Task] -> Html
renderForm relation tasks = [hsx|
    <form method="POST" action={CreateRelationAction}>
        <label>Task</label>
        <div class="form-group">
            <select name="dependee" class="form-control" required>
                {renderOptions (get #dependee relation) tasks}
            </select>
        </div>
        <label>depends on</label>
        <div class="form-group">
            <select name="dependency" class="form-control" required>
                {renderOptions (get #dependency relation) tasks}
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
|]

renderOptions :: Id Task -> [Task] -> Html
renderOptions selected tasks = [hsx|
        {forEach tasks (\ task -> 
            if (get #id task) == selected 
                then renderSelected task
                else renderSelect   task
            )
        }
    |]

renderSelect :: Task -> Html
renderSelect task = [hsx|
        <option value={show $ get #id task}>
            {get #title task}
        </option>
    |]

renderSelected :: Task -> Html
renderSelected task = [hsx|
        <option value={show $ get #id task} selected>
            {get #title task}
        </option>
    |]

