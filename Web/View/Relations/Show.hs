module Web.View.Relations.Show where
import Web.View.Prelude

data ShowView = ShowView { relation :: Relation }

instance View ShowView where
    html ShowView { .. } = [hsx||]
