module Web.Routes where
import IHP.RouterPrelude
import Generated.Types
import Web.Types

-- Generator Marker
instance AutoRoute StaticController
instance AutoRoute TasksController
instance AutoRoute RelationsController
instance AutoRoute UserController
instance AutoRoute SessionsController

