module Web.Types where

import IHP.Prelude
import IHP.ModelSupport
import IHP.LoginSupport.Types
import IHP.View.Form
import Generated.Types

import Data.UUID ( fromText, nil )

toUUID :: (Show a) => a -> UUID
toUUID = purify . fromText . show
    where
        purify (Just id) = id
        purify Nothing = nil

data WebApplication = WebApplication deriving (Eq, Show)

data StaticController = WelcomeAction deriving (Eq, Show, Data)

instance CanSelect Bool where
    type SelectValue Bool = Bool
    selectValue value = value
    selectLabel False = "Todo"
    selectLabel True  = "Done"

instance CanSelect Task where
    type SelectValue Task = Id Task
    selectValue = get #id
    selectLabel = get #title

{-
instance SetField "dependee" Relation UUID where
    {-# INLINE setField #-}
    setField newValue (Relation dependency dependee meta) =
        Relation dependency newValue (meta { touchedFields = "dependee" : touchedFields meta })

instance SetField "dependency" Relation UUID where
    {-# INLINE setField #-}
    setField newValue (Relation dependency dependee meta) =
        Relation newValue dependee (meta { touchedFields = "dependency" : touchedFields meta })

instance SetField "meta" Relation MetaBag where
    {-# INLINE setField #-}
    setField newValue (Relation dependency dependee meta) =
        Relation dependency dependee newValue
-}
instance HasNewSessionUrl User where
    newSessionUrl _ = "/NewSession"

type instance CurrentUserRecord = User

data TasksController
    = TasksAction        
    | SortedTasksAction  { ord :: Text }
    | NewTaskAction
    | ShowTaskAction     { taskId :: !(Id Task) }
    | CreateTaskAction
    | EditTaskAction     { taskId :: !(Id Task) }
    | UpdateTaskAction   { taskId :: !(Id Task) }
    | ToggleStatusAction { taskId :: !(Id Task), redirect :: !(Text) }
    | DeleteTaskAction   { taskId :: !(Id Task) }
    deriving (Eq, Show, Data)

data RelationsController
    = RelationsAction
    | NewRelationAction
    | CreateRelationAction
    | AddRelationAction    { dependee :: !(Id Task), dependency :: !(Id Task) }
    | DeleteRelationAction { dependee :: !(Id Task), dependency :: !(Id Task) }
    deriving (Eq, Show, Data)

data UserController
    = NewUserAction
    | ShowUserAction
    | CreateUserAction
    | EditUserAction
    | UpdateUserAction
    | DeleteUserAction
    | ConfirmUserAction { userId :: !(Id User), confirmationToken :: !UUID }
    deriving (Eq, Show, Data)

data SessionsController
    = NewSessionAction
    | CreateSessionAction
    | DeleteSessionAction
    deriving (Eq, Show, Data)
