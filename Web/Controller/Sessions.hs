module Web.Controller.Sessions where

import Web.Controller.Prelude
import Web.View.Sessions.New
import qualified IHP.AuthSupport.Controller.Sessions as Sessions
import qualified IHP.AuthSupport.Confirm as Confirm

instance Controller SessionsController where
    action NewSessionAction    = Sessions.newSessionAction    @User
    action CreateSessionAction = Sessions.createSessionAction @User
    action DeleteSessionAction = do
        setSuccessMessage "Logged out"
        Sessions.deleteSessionAction @User

instance Sessions.SessionsControllerConfig User where
    beforeLogin user = do
        Confirm.ensureIsConfirmed user
