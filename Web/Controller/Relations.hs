module Web.Controller.Relations where

import Web.Controller.Prelude
import Web.View.Tasks.Index
import Web.View.Relations.New


instance Controller RelationsController where
    action RelationsAction = do
        redirectTo TasksAction

    action NewRelationAction = do
        tasks <- query @Task
            |> orderBy #title
            |> fetch
        let relation = newRecord
        render NewView { .. }

    action CreateRelationAction = do
        tasks <- query @Task
            |> fetch

        let dependency :: Id Task = param "dependency"
        let dependee   :: Id Task = param "dependee"

        dependees :: [Id Task] <- getAllDependees $ getDirectDependees dependee

        let task = newRecord @Relation
        task
            |> buildRelation
            |> validateField #dependency (isUnequalTo dependee)
            |> validateField #dependee   (isUnequalTo dependency)
            |> validateField #dependency (notInList   dependees)
            |> ifValid \case
                Left relation -> render NewView { .. }
                Right relation -> do
                    relation <- relation |> createRecord
                    setSuccessMessage "Tasks related"
                    redirectTo TasksAction

    action AddRelationAction    { dependee, dependency } = do
        tasks <- query @Task
                |> orderBy #title
                |> fetch

        let relation = newRecord @Relation
                |> set #dependee dependee
                |> set #dependency dependency
        
        render NewView { .. }

    action DeleteRelationAction { dependee, dependency } = do
        task <- query @Task
            |> filterWhere (#id, dependee)
            |> fetchOne
        authorise task currentUserId do
            relation <- query @Relation
                |> filterWhere (#dependee, dependee)
                |> filterWhere (#dependency, dependency)
                |> fetchOne
            deleteRecord relation
            setSuccessMessage "Relation deleted"
            redirectTo RelationsAction

buildRelation relation = relation
    |> fill @["dependency", "dependee"]

isUnequalTo :: (Eq a) => a -> a -> ValidatorResult
isUnequalTo a b | a == b    = Failure "A Task can't depend upon itself"
                | otherwise = Success

notInList :: [Id Task] -> Id Task -> ValidatorResult
notInList ts t  | t `elem` ts = Failure "Circular dependencies are not allowed"
                | otherwise   = Success

authorise :: (?context::ControllerContext) => Task -> Id User -> IO () -> IO ()
authorise task userId action = do
    if (get #owner task) /= userId
        then do
            setErrorMessage "Authorisation error"
            redirectTo TasksAction
        else action
