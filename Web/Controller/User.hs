module Web.Controller.User where

import Web.Controller.Prelude hiding ( sendMail )
import Web.View.User.New
import Web.View.User.Edit
--import Web.Mail.Users.Confirmation
import IHP.AuthSupport.Confirm
import IHP.ValidationSupport.ValidateIsUnique ( withCustomErrorMessageIO )
import qualified IHP.AuthSupport.Controller.Confirmations as Confirmations
import qualified Web.Controller.Sessions ()

import Network.SendGridV3.Api

import Data.List.NonEmpty ( fromList )

instance Confirmations.ConfirmationsControllerConfig User where

instance Controller UserController where
    action NewUserAction = do
        let user = newRecord
        render NewView { .. }

    action ShowUserAction = do
        redirectTo TasksAction

    action EditUserAction = do
        user <- fetch currentUserId
        render EditView { .. }

    action UpdateUserAction = do
        let password     :: Text = param "password"
        let confirmation :: Text = param "confirmation"

        user <- fetch currentUserId
        user
            |> set #email (param "email" :: Text)
            |> validateField #passwordHash (hasMinLength 8)
            |> validateField #passwordHash (password `isEqualTo` confirmation)
            |> validateField #email nonEmpty
            |> validateField #email isEmail
            |> withCustomErrorMessageIO "Email is already in use" validateIsUniqueCaseInsensitive #email
            >>= ifValid \case
                Left user -> do
                    setErrorMessage "Passwords do not match"
                    redirectTo EditUserAction
                Right user -> do
                    hashed <- hashPassword password
                    user <- user
                        |> set #passwordHash 
                            (if null password 
                                then (get #passwordHash user)
                                else hashed)
                        |> updateRecord


                    setSuccessMessage "User updated"
                    redirectTo $ SortedTasksAction "title"

    action CreateUserAction = do
        let user = newRecord @User
        user
            |> fill @["email", "passwordHash"]
            |> validateField #passwordHash nonEmpty
            |> validateField #passwordHash (hasMinLength 8)
            |> validateField #email isEmail
            |> withCustomErrorMessageIO "Email is already in use" validateIsUniqueCaseInsensitive #email
            >>= ifValid \case
                Left user -> render NewView { .. } 
                Right user -> do
                    hashed <- hashPassword (get #passwordHash user)
                    user <- user 
                        |> set #passwordHash hashed
                        |> createRecord

                    --sendConfirmationMail user
                    let msg = "Please click the following link to finish account creation:\n\n" ++ (urlTo $ ConfirmUserAction (get #id user) (get #confirmationToken user))

                    sendMail sendGridApiKey (createMail (get #email user) msg)

                    setSuccessMessage ("Please follow the confirmation link we sent via mail to " ++ (get #email user))
                    redirectTo NewSessionAction

    action ConfirmUserAction { userId, confirmationToken } = do
        user :: User <- fetch userId

        if (get #confirmationToken user) == confirmationToken
            then do
                user
                    |> set #isConfirmed True
                    |> updateRecord
                setSuccessMessage ("Account " ++ (get #email user) ++ " was verified successfully!")
            else do
                setErrorMessage ("Account verification for " ++ (get #email user) ++ " failed!")

        redirectTo NewSessionAction

        
        {-
        user
            |> validateField #confirmationToken (isInList [confirmationToken])
            >>= ifValid \case
                Left user -> do 
                    setErrorMessage ("Account verification for " ++ (get #email user) ++ " failed!")
                    redirectTo NewSessionAction

                Right user -> do
                    user <- user
                        |> set #isConfirmed True
                        |> updateRecord
                    setSuccessMessage ("Account " ++ (get #email user) ++ " was verified successfully!")
                    redirectTo NewSessionAction
        -}
        redirectTo NewSessionAction


    action DeleteUserAction = do
        user <- fetch currentUserId
        deleteRecord user
        setSuccessMessage "User deleted"
        redirectTo NewSessionAction

isEqualTo :: (Eq a) => a -> a -> b -> ValidatorResult
isEqualTo a b _ | a == b    = Success
                | otherwise = Failure "The passwords do not match"

sendGridApiKey :: ApiKey
sendGridApiKey = ApiKey "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"


createMail :: (?modelContext :: ModelContext) => Text -> Text -> Mail () ()
createMail email msg =
    let to = personalization $ fromList [MailAddress email ""]
        from = MailAddress "aaron.alef@code.berlin" "eqq"
        subject = "Confirm your account"
        content = Just $ fromList [mailContentText msg]
    in mail [to] from subject content

