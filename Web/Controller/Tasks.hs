module Web.Controller.Tasks where

import Web.Controller.Prelude
import Application.Helper.Controller
import Web.View.Tasks.Index
import Web.View.Tasks.New
import Web.View.Tasks.Edit
import Web.View.Tasks.Show

import Data.UUID (nil)

instance Controller TasksController where
    action TasksAction = do
        redirectTo $ SortedTasksAction "title"

    action SortedTasksAction { ord } = do
        tasks <- query @Task
            |> filterWhere (#owner, currentUserId)
            |> orderByCol ord
            |> orderBy #done
            |> orderBy #dueAt
            |> orderBy #title
            |> fetch
        render IndexView { .. }

    action NewTaskAction = do
        let task = newRecord
        render NewView { .. }

    action ShowTaskAction { taskId } = do
        task <- fetch taskId
        authorise task currentUserId do
            dependencyIds <- getAllDependencies $ getDirectDependencies taskId
            dependencies  <- query @Task
                    |> filterWhereIn (#id, dependencyIds)
                    |> orderBy #title
                    |> fetch

            dependeeIds   <- getAllDependees $ getDirectDependees taskId
            dependees     <- query @Task
                    |> filterWhereIn (#id, dependeeIds)
                    |> orderBy #title
                    |> fetch

            render ShowView { .. }

    action EditTaskAction { taskId } = do
        task <- fetch taskId
        authorise task currentUserId
                $ render EditView { .. }

    action UpdateTaskAction { taskId } = do
        task <- fetch taskId
        authorise task currentUserId do
            task <- task 
                |> buildTask
                |> updateRecord
            setSuccessMessage "Task updated"
            redirectTo ShowTaskAction { .. }

    action ToggleStatusAction { taskId, redirect } = do
        putStrLn ("Request:\t"  <> show getRequestPathAndQuery)
        putStrLn ("Redirect:\t" <> redirect)

        task <- fetch taskId
        authorise task currentUserId do
            let status = not $ get #done task
            task 
                |> set #done status
                |> updateRecord
            redirectToPath redirect

    action CreateTaskAction = do
        let task = newRecord @Task
        task
            |> buildTask
            |> set #owner currentUserId
            |> ifValid \case
                Left task -> render NewView { .. } 
                Right task -> do
                    task <- task |> createRecord
                    setSuccessMessage "Task created"
                    redirectTo TasksAction

    action DeleteTaskAction { taskId } = do
        task <- fetch taskId
        authorise task currentUserId do
            deleteRecord task
            setSuccessMessage "Task deleted"
            redirectTo $ SortedTasksAction "title"

buildTask task = task
    |> fill @["title","description","done","createdAt","dueAt"]

authorise task userId action = do
    if (get #owner task) /= userId
        then do
            setErrorMessage "Authorisation error"
            redirectTo TasksAction
        else action
