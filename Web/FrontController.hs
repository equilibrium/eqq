module Web.FrontController where

import IHP.RouterPrelude
import IHP.LoginSupport.Middleware
import Web.Controller.Prelude
import Web.Controller.Sessions
import Web.View.Layout (defaultLayout)

-- Controller Imports
import Web.Controller.User
import Web.Controller.Relations
import Web.Controller.Relations
import Web.Controller.Tasks
import Web.Controller.Relations
import Web.Controller.Relations
import Web.Controller.Tasks
import Web.Controller.Static

instance FrontController WebApplication where
    controllers = 
        [ startPage TasksAction
        -- Generator Marker
        , parseRoute @UserController
        , parseRoute @RelationsController
        , parseRoute @SessionsController
        , parseRoute @TasksController
        ]

instance InitControllerContext WebApplication where
    initContext = do
        setLayout defaultLayout
        initAutoRefresh
        initAuthentication @User
