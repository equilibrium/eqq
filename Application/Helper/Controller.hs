module Application.Helper.Controller where

import IHP.ControllerPrelude
import Web.Types
import Generated.Types

-- Here you can add functions which are available in all your controllers

{--
toUUID :: (Show a) => a -> UUID
toUUID = purify . fromText . show
    where
        purify (Just id) = id
        purify Nothing = nil
--}

orderByCol "status"  = orderBy #done
orderByCol "due_at" = orderBy #dueAt
orderByCol "title" = orderBy #title
orderByCol _  = orderBy #done

getDirectDependencies :: (?modelContext :: ModelContext) => Id Task -> IO [Id Task]
getDirectDependencies taskId = map (Id . toUUID . get #dependency)
    <$> query @Relation
        |> findManyBy #dependee taskId

getDirectDependees :: (?modelContext :: ModelContext) => Id Task -> IO [Id Task]
getDirectDependees taskId = map (Id . toUUID . get #dependee)
    <$> query @Relation
        |> findManyBy #dependency taskId

getAllDependencies :: (?modelContext :: ModelContext) => IO [Id Task] -> IO [Id Task]
getAllDependencies taskIds = do
    let accumulateDeps task = ((++) <$> getDirectDependencies task <*>)

    tasks <- nub 
        <$> taskIds
    indirectDeps <- nub 
        <$> foldr accumulateDeps (pure tasks) tasks

    if tasks == indirectDeps 
        then return tasks
        else getAllDependencies (pure indirectDeps)

getAllDependees :: (?modelContext :: ModelContext) => IO [Id Task] -> IO [Id Task]
getAllDependees taskIds = do
    let accumulateDeps task = ((++) <$> getDirectDependees task <*>)

    tasks <- nub 
        <$> taskIds
    indirectDeps <- nub 
        <$> foldr accumulateDeps (pure tasks) tasks

    if tasks == indirectDeps 
        then return tasks
        else getAllDependees (pure indirectDeps)
