module Application.Helper.View where

import IHP.ViewPrelude
import Generated.Types

import qualified Data.Text as T ( unpack
                                , init
                                , tail
                                )

-- Here you can add functions which are available in all your viewsimport Generated.Types  ( StatusType )


renderRequiredLabel :: String -> Html
renderRequiredLabel title = [hsx|<label><span title="Required">{title ++ " *"}</span></label>|]

statusBgColor :: Task -> String
statusBgColor task = colorOf (get #done task)
    where
        colorOf False = "background-color:#FF1111"
        colorOf True  = "background-color:#00FF00"

divStatus :: Task -> Html
divStatus task = [hsx|
    <div id={statusId task} style="width: 50px">
        {statusText task}
    </div>
|]

statusText :: Task -> String
statusText task = statusOf (get #done task)
    where
        statusOf False = "Todo"
        statusOf True  = "Done"
statusId :: Task -> String
statusId task = idOf (get #done task)
    where
        idOf False = "status-todo"
        idOf True  = "status-done"

textBody :: Text -> Text
textBody = T.init . T.tail
