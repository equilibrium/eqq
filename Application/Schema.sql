-- Your database schema. Use the Schema Designer at http://localhost:8001/ to add some tables.
CREATE TABLE tasks (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    title TEXT NOT NULL,
    description TEXT DEFAULT '' NOT NULL,
    done BOOLEAN DEFAULT false NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
    due_at TIMESTAMP WITH TIME ZONE DEFAULT NULL,
    owner UUID NOT NULL
);
CREATE TABLE relations (
    dependency UUID NOT NULL,
    dependee UUID NOT NULL,
    PRIMARY KEY(dependency, dependee)
);
CREATE TABLE users (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    email TEXT NOT NULL,
    password_hash TEXT NOT NULL,
    locked_at TIMESTAMP WITH TIME ZONE DEFAULT NULL,
    failed_login_attempts INT DEFAULT 0 NOT NULL,
    confirmation_token UUID DEFAULT uuid_generate_v4() NOT NULL,
    is_confirmed BOOLEAN DEFAULT false NOT NULL
);
ALTER TABLE relations ADD CONSTRAINT relations_ref_dependee   FOREIGN KEY (dependee)   REFERENCES tasks (id) ON DELETE CASCADE;
ALTER TABLE relations ADD CONSTRAINT relations_ref_dependency FOREIGN KEY (dependency) REFERENCES tasks (id) ON DELETE CASCADE;
ALTER TABLE tasks     ADD CONSTRAINT tasks_ref_owner          FOREIGN KEY (owner)      REFERENCES users (id) ON DELETE CASCADE;
