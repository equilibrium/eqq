

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;


SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE public.users DISABLE TRIGGER ALL;



ALTER TABLE public.users ENABLE TRIGGER ALL;


ALTER TABLE public.tasks DISABLE TRIGGER ALL;

INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000001', 'a', '', false, '2021-04-27 00:01:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000003', 'c', '', false, '2021-04-27 00:03:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000005', 'e', '', false, '2021-04-27 00:05:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000007', 'g', '', false, '2021-04-27 00:07:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000009', 'i', '', false, '2021-04-27 00:09:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000011', 'k', '', false, '2021-04-27 00:11:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000013', 'm', '', false, '2021-04-27 00:13:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000015', 'o', '', false, '2021-04-27 00:15:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000017', 'q', '', false, '2021-04-27 00:17:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000019', 's', '', false, '2021-04-27 00:19:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000021', 'u', '', false, '2021-04-27 00:21:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000023', 'w', '', false, '2021-04-27 00:23:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000025', 'y', '', false, '2021-04-27 00:25:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000002', 'b', '', true, '2021-04-27 00:02:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000004', 'd', '', true, '2021-04-27 00:04:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000006', 'f', '', true, '2021-04-27 00:06:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000008', 'h', '', true, '2021-04-27 00:08:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000010', 'j', '', true, '2021-04-27 00:10:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000012', 'l', '', true, '2021-04-27 00:12:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000014', 'n', '', true, '2021-04-27 00:14:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000016', 'p', '', true, '2021-04-27 00:16:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000018', 'r', '', true, '2021-04-27 00:18:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000020', 't', '', true, '2021-04-27 00:20:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000022', 'v', '', true, '2021-04-27 00:22:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000024', 'x', '', true, '2021-04-27 00:24:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');
INSERT INTO public.tasks (id, title, description, done, created_at, due_at, owner) VALUES ('00000000-0000-0000-0000-000000000026', 'z', '', true, '2021-04-27 00:26:00+02', NULL, '95d3e14a-c118-45a2-81d5-9905f43cd585');


ALTER TABLE public.tasks ENABLE TRIGGER ALL;


ALTER TABLE public.relations DISABLE TRIGGER ALL;

INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000002', '00000000-0000-0000-0000-000000000001');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000004', '00000000-0000-0000-0000-000000000001');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000005', '00000000-0000-0000-0000-000000000001');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000011', '00000000-0000-0000-0000-000000000001');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000014', '00000000-0000-0000-0000-000000000001');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000003', '00000000-0000-0000-0000-000000000002');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000014', '00000000-0000-0000-0000-000000000002');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000015', '00000000-0000-0000-0000-000000000002');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000023', '00000000-0000-0000-0000-000000000002');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000025', '00000000-0000-0000-0000-000000000002');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000008', '00000000-0000-0000-0000-000000000003');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000009', '00000000-0000-0000-0000-000000000003');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000010', '00000000-0000-0000-0000-000000000003');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000011', '00000000-0000-0000-0000-000000000003');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000012', '00000000-0000-0000-0000-000000000003');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000026', '00000000-0000-0000-0000-000000000004');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000011', '00000000-0000-0000-0000-000000000005');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000012', '00000000-0000-0000-0000-000000000005');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000013', '00000000-0000-0000-0000-000000000005');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000014', '00000000-0000-0000-0000-000000000005');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000015', '00000000-0000-0000-0000-000000000005');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000016', '00000000-0000-0000-0000-000000000005');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000017', '00000000-0000-0000-0000-000000000005');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000018', '00000000-0000-0000-0000-000000000005');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000019', '00000000-0000-0000-0000-000000000005');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000020', '00000000-0000-0000-0000-000000000005');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000016', '00000000-0000-0000-0000-000000000006');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000006', '00000000-0000-0000-0000-000000000007');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000013', '00000000-0000-0000-0000-000000000008');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000014', '00000000-0000-0000-0000-000000000009');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000018', '00000000-0000-0000-0000-000000000009');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000019', '00000000-0000-0000-0000-000000000009');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000020', '00000000-0000-0000-0000-000000000009');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000011', '00000000-0000-0000-0000-000000000010');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000012', '00000000-0000-0000-0000-000000000010');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000013', '00000000-0000-0000-0000-000000000010');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000017', '00000000-0000-0000-0000-000000000011');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000018', '00000000-0000-0000-0000-000000000011');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000019', '00000000-0000-0000-0000-000000000011');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000020', '00000000-0000-0000-0000-000000000011');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000021', '00000000-0000-0000-0000-000000000011');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000023', '00000000-0000-0000-0000-000000000011');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000024', '00000000-0000-0000-0000-000000000011');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000025', '00000000-0000-0000-0000-000000000012');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000026', '00000000-0000-0000-0000-000000000012');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000026', '00000000-0000-0000-0000-000000000013');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000025', '00000000-0000-0000-0000-000000000014');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000024', '00000000-0000-0000-0000-000000000015');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000017', '00000000-0000-0000-0000-000000000016');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000018', '00000000-0000-0000-0000-000000000016');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000019', '00000000-0000-0000-0000-000000000017');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000020', '00000000-0000-0000-0000-000000000018');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000021', '00000000-0000-0000-0000-000000000018');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000022', '00000000-0000-0000-0000-000000000019');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000022', '00000000-0000-0000-0000-000000000020');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000022', '00000000-0000-0000-0000-000000000021');
INSERT INTO public.relations (dependency, dependee) VALUES ('00000000-0000-0000-0000-000000000023', '00000000-0000-0000-0000-000000000022');


ALTER TABLE public.relations ENABLE TRIGGER ALL;


