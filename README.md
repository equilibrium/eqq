# eqq

A task manager in progress, written in Haskell using [IHP](https://ihp.digitallyinduced.com/) and deployed via [IHP Cloud](https://ihpcloud.com/).

Find a deployed instance here: https://lqlobxbcuqzbkpncimsrwbmsfybrbnhl.ihpapp.com/

## About

Eqq is a _very_ simple, work-in-progress task manager that focuses on tasks being dependent onto each other. Common practices like Kanban boards typically fail to properly display dependencies between tasks that need to be resolved in order to decide what next to work on.
To solve this, the goal of this project is to provide a clean user interface allowing to
[ ] view resolved task dependency trees for a quick overview as to what is blocking what
[ ] resolve mash-ups of tasks from different areas (work, choires, shopping lists...) into dependency trees independent from each other

Note that both points are still WIP.

The way it's intended is that each task has a few very basic properties (title, description, due date) and everything else is modeled as dependency relations between two tasks.

## Setup

### Prerequisites

This project is built using IHP which uses [nix](https://nixos.org/manual/nix/) as package manager for Haskell.
Thus, nix is required to build the project. It ensures the integrity of the haskell compiler and all needed packages.
You may refer to [IHP installation Guide #Nix](https://ihp.digitallyinduced.com/Guide/installation.html#1-dependency-nix-package-manager) for instructions on how to set nix up.
Additionally, [direnv](https://direnv.net) is needed.
IHP itself is _not_ required to run this project as all its libraries get installed by nix.

### Build the project

Clone the repository:
```
git clone https://codeberg.org/equilibrium/eqq
```
Change working directory to the project folder `eqq` and start the server:
```
cd eqq
./start
```
Nix will now start installing all dependencies, build the project and start the server.
The running instance can then be found at localhost:8000.
An administrative instance giving access to the PostgreSQL database can be found at localhost:8001.

### Notes
* If port 8000 or 8001 is blocked already the project will be running on the next free ports. `./start` should print the corresopnding address after it finished building.
* Running the project for the first time takes some time (10~15 minutes) for nix to download and compile.
* Nix requires a lot of disk space to install the packages. If it fails to build check available space and retry.

## Structure

The project currently consists of a `Web` application only. Everything relevant to it can be found in the `Web/` folder.
It's as monolithic as one can get in web development; no external services are being used, it's intended to run on just one server (although both Postgres and IHP make it rather easy to run it on several servers in parallel).
In the end, all this is just a setup to provide a nice user interface to a database.

##### Structural diagram
![Structure](docs/structure.png)

### Architecture

Eqq uses a Model-View-Controller architecture:
* Model: A database serves as data storage; the data format is defined by its schema.
* View: The views are found under `Web/View/`. It uses HSX (JSX for Haskell) to generate HTML and JavaScript code.
  * In `Tasks/` are most of the views like creating, displaying and managing your tasks.
  * `Relations/` only serves to render forms to create and edit relations.
* Controller: All server logic sits in `Web/Controller/`. It mainly converts HTML forms into SQL queries and turns SELECT queries into useful data to be displayed.
  * Due to Haskell being a purely functional programming language access to the database has to be passed to each function using it, ensuring the database can only be accessed by the controllers.

### Database

All data is stored in a PostgreSQL database. Its schema, found in `Application/Schema.sql`, defines both the table setup and is used to generate the Haskell data types.
The schema is rather simple, as the database only needs to store tasks and their relations. The major challenge was to model and work with the many-to-many relationship that arose from the given network recursion.
For example, given a huge amount of tasks and their relations, one can't just join both tables together as this might potentially lead to severe performance issues. Rather, one has to filter the Relations table for the dependency or the dependee first and then do a corresponding inner join.

## Security

### Accounts and email
Usage of this service requires the creation and use of a user account.
Accounts use an email address for signup and login, hence only one account may exist per email address. This email address may be switched at any point with a different unique address (i.e., the email is _not_ the id but serves as login name for the account).
To successfully create an account. the email used needs to be verified. Email validation consists of sending a special link that, once clicked, confirms that the email account belongs to the current user. After verification, the user needs to login.

Once logged in, session may be stored as cookie in the browser and persists for maximum 30 days.


### Password protection

Accounts are currently protected using single-factor authentication. The web interface sends submitted passwords to the server in plain text. The responsibility of making sure the password is sent to the server securely lies with the user; https is enforced however. Hashing the passwords on the server prevents potential brute force attempts through powerful external hardware; attackers are bound by the performance of the server.
Passwords are hashed by using SHA256 with random salt for 2^17 = 131072 iterations.
Passwords are required to be at least 8 characters long, no further restrictions apply.
After 10 failed login attempts, an account gets locked for 1 hour.


### Service

Tasks and their relations created using this service belong to the creator account. They can only be viewed, modified and administrated by that account. The same rules apply to relations between tasks, and any potential additional services in the future.


### Web server

The project uses server-side rendering. Therefore, the state of the web site is maintained on the server also. This is opposed to the currently widespread practice of rendering web sites in the browser, mostly using JavaScript. Especially third-party JavaScript libraries may contain security vulnerabilities. Haskell third-party libraries are typically much more secure, by pure virtue of being functional.
Furthermore, the service focusses on simplicity - any service used potentially adds flaws. A possible alternative for the login process would be login over Google or GitHub. This would have the advantage of offering two-factor-authentication while ensuring no user data is stored on my server instance whatsoever. The important issue is, however, that I, as service provider, am responsible for personal data on the servers of big tech companies (Google, Microsoft).


### Database: PostgreSQL

The Postgres server sits in the same instance as the web server and does not allow TCP connections. All data is therefore behind another layer of abstractions; all requests have to pass through the web server.
All forms issued by users are first converted to Haskell types. Haskell's strong typing system prevents abuse in the form of SQL injection - at no point are queries from the user redirected to the database without type-level validation.


### Permission

As of now, this service prevents harm from hacked administrator accounts or elevation of privileges by not including omnipotent administrator accounts. No person should have the rights to access any account freely. As this is a free service, there are no guarantees for the consistency of service or accounts. It is a user's responsibility to ensure access to everything needed for authentication and authorisation on this web site, namely to email account and password provided.


### Threat model

To better understand potential threats I've first created a Data Flow Diagram of the service:

![Data Flow Diagram](docs/DFD.png)

Based on this, there are several possible attack vectors:

* (Dedicated) Denial of Service: Too many simultaneous requests could render the service unusable. However, this service is deployed over AWS and therefore protected by Amazon's standard DDoS protection [Amazon Shield](https://aws.amazon.com/shield/).
* Submission of malicious code (XSS or SQL injection) into register or login forms: As mentioned above, Haskell's strong type system and mapping of forms into the type system make these kinds of attaks hard
* Guessing of user passwords: Brute force is possible but very slow. Dictionary attacks are a potential vulnerability, though.
* Intercepting user passwords: Nearly impossible through asymmetric encryption between browser and server (HTTPS).
* Guessing hyper links: Checks are in place to prevent anyone from accessing tasks or user data unauthorised, even with knowledge of specific user or task UUIDs.

----

Copyright © 2021 Aaron Alef <aaron.alef@code.berlin> [@anarchuser](https://codeberg.org/anarchuser)
