let
    ihp = builtins.fetchTarball {
        url = "https://ihp.digitallyinduced.com/BuildTarball?userId=b42cade0-d183-455f-8420-e51aa248e76f&token=jTaDemmpWPIavoJbLwJJBHPeLsYltuew&version=ac9d1c4aba3c7db653b27ab2119585624b60c5f6";
        sha256 = "1f4qnfqdabgfqh1lx3n0nmk0pk88j8a4yn5din9v67ryy4apz8yg";
    };
    haskellEnv = import "${ihp}/NixSupport/default.nix" {
        ihp = ihp;
        haskellDeps = p: with p; [
            cabal-install
            base
            wai
            text
            hlint
            p.ihp
            mmark

            process
            # sendgrid-v3
        ];
        otherDeps = p: with p; [
            mailutils
            glibc
        ];
        projectPath = ./.;
    };
in
    haskellEnv
